var csvjson = require('csvjson');
const fs=require('fs')
const csvFilePath='../data/deliveries.csv'
var data = fs.readFileSync(csvFilePath, { encoding : 'utf8'});
/*
{
    delimiter : <String> optional default is ","
    quote     : <String|Boolean> default is null
}
*/
var options = {
  delimiter : ',', // optional
  quote     : '"' // optional
};
// for multiple delimiter you can use regex pattern like this /[,|;]+/
 
/* 
  for importing headers from different source you can use headers property in options 
  var options = {
    headers : "sr,name,age,gender"
  };
*/
 
const deliveries=csvjson.toObject(data, options);

//console.log(deliveries);

//for match
const csVFilePath='../data/matches.csv'
var dAta = fs.readFileSync(csVFilePath, { encoding : 'utf8'});
/*
{
    delimiter : <String> optional default is ","
    quote     : <String|Boolean> default is null
}
*/
var options = {
  delimiter : ',', // optional
  quote     : '"' // optional
};
// for multiple delimiter you can use regex pattern like this /[,|;]+/
 
/* 
  for importing headers from different source you can use headers property in options 
  var options = {
    headers : "sr,name,age,gender"
  };
*/
 
const matches=csvjson.toObject(dAta, options);

//console.log(matches);

//const fs = require('fs');

//function 1
const matchesPerYear  = require('./matchesPerYear.js');
const result1=matchesPerYear(matches);


let data1 = JSON.stringify(result1);
fs.writeFile("../public/output/matchesPerYear.json", data1, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/matchesPerYear.json", "utf8"));
  }
});

//function 2
const matchesWonByPerTeamPerYear  = require('./matchesWonByPerTeamPerYear.js');
const result2=matchesWonByPerTeamPerYear(matches);
//console.log(result2);

let data2 = JSON.stringify(result2);
fs.writeFile("../public/output/matchesWonByPerTeamPerYear.json", data2, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/matchesWonByPerTeamPerYear.json", "utf8"));
  }
});

//function 3
const extraRunsConcededPerTeam = require('./extraRunsConcededPerTeam.js');
const result3=extraRunsConcededPerTeam(matches,deliveries,2016);

let data3 = JSON.stringify(result3);
fs.writeFile("../public/output/extraRunsConcededPerTeam.json", data3, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/extraRunsConcededPerTeam.json", "utf8"));
  }
});

//function 4

const topTenEconomicalBowlers = require('./topTenEconomicalBowlers.js');
const result4=topTenEconomicalBowlers(matches,deliveries,2015);


let data4 = JSON.stringify(result4);
fs.writeFile("../public/output/topTenEconomicalBowlers.json", data4, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/topTenEconomicalBowlers.json", "utf8"));
  }
});

//function 5

const teamWonTossWonMacth= require('./teamWonTossWonMacth.js');
const result5=teamWonTossWonMacth(matches);
//console.log(result5);
let data5 = JSON.stringify(result5);
fs.writeFile("../public/output/teamWonTossWonMacth.json", data5, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/teamWonTossWonMacth.json", "utf8"));
  }
});

//function 6

const playerOfTheMatchForEachSeason= require('./playerOfTheMatchForEachSeason.js');
const result6=playerOfTheMatchForEachSeason(matches);
//console.log(result6);
let data6 = JSON.stringify(result6);
fs.writeFile("../public/output/playerOfTheMatchForEachSeason.json", data6, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/playerOfTheMatchForEachSeason.json", "utf8"));
  }
});

//function 7


const strikeRateBatsmanEachSeason= require('./strikeRateBatsmanEachSeason.js');
const result7=strikeRateBatsmanEachSeason(matches,deliveries);
//console.log(result7);
let data7 = JSON.stringify(result7);
fs.writeFile("../public/output/strikeRateBatsmanEachSeason.json", data7, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/strikeRateBatsmanEachSeason.json", "utf8"));
  }
});


//function 8
const highestNumberOfTimesPlayerDismissed= require('./highestNumberOfTimesPlayerDismissed.js');
const result8=highestNumberOfTimesPlayerDismissed(deliveries);
//console.log(result8);
let data8 = JSON.stringify(result8);
fs.writeFile("../public/output/highestNumberOfTimesPlayerDismissed.json", data8, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/highestNumberOfTimesPlayerDismissed.json", "utf8"));
  }
});

//function 9
const bowlerBestEconomyInSuperOver= require('./bowlerBestEconomyInSuperOver.js');
const result9=bowlerBestEconomyInSuperOver(deliveries);
//console.log(result9);
let data9 = JSON.stringify(result9);
fs.writeFile("../public/output/bowlerBestEconomyInSuperOver.json", data9, (err) => {
  if (err)
    console.log(err);
  else {
    console.log("File written successfully\n");
    console.log("The written has the following contents:");
    console.log(fs.readFileSync("../public/output/bowlerBestEconomyInSuperOver.json", "utf8"));
  }
});



