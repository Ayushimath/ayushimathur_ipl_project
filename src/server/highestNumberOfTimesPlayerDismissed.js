const topTenEconomicalBowlers = require("./topTenEconomicalBowlers");

function highestNumberOfTimesPlayerDismissed(deliveries) {
   
    let record = {};
    
    for (let eachdelivery of deliveries) {
        let playerdismissed=eachdelivery.player_dismissed;
        let bowler=eachdelivery.bowler;
        let dismissalkind=eachdelivery.dismissal_kind;

        if (playerdismissed !== "" && dismissalkind!=='run out' && dismissalkind!=='retired hurt' && 
        dismissalkind!=='obstructing the field' && dismissalkind!=='hit wicket') {
            if (!record.hasOwnProperty(playerdismissed)) {
                record[playerdismissed] = {};
                record[playerdismissed][bowler] = 1;

            }
            else if (record.hasOwnProperty(playerdismissed)) {
                if (record[playerdismissed].hasOwnProperty(bowler)) {
                    record[playerdismissed][bowler] += 1;
                } else {
                    record[playerdismissed][bowler]= 1;
                }
            }
        }
    }
    let resultArray = [];
    for (let playerdismissed in record) {

        for (let bowler in record[`${playerdismissed}`]) {
            let result = [];
            result.push(playerdismissed);
            result.push(bowler);
            result.push(record[playerdismissed][bowler])
            resultArray.push(result);
        }
    }
    let result = resultArray.sort(function (a, b) {
        return b[2] - a[2];
    });
    return result[0];
}
module.exports = highestNumberOfTimesPlayerDismissed;