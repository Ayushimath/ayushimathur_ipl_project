

function extraRunsConcededPerTeam(matches,deliveries,year){
    object = {}

    //iterating through object in array
    let matchid;
    for (let i = 0; i < matches.length; i++) {
        if(matches[i]["season"]==year){
             matchid=matches[i]["id"]
            
            for(let j=0;j<deliveries.length;j++){
               
                if(deliveries[j]["match_id"]===matchid){
                    
                    if(`${deliveries[j]["bowling_team"]}`in object){
                        
                        object[`${deliveries[j]["bowling_team"]}`]+=parseInt(deliveries[j]["extra_runs"]);

                    } else{
                        object[`${deliveries[j]["bowling_team"]}`]=parseInt(deliveries[j]["extra_runs"]);
                    }


                }
            }
        }
    }
    return(object);

}
module.exports=extraRunsConcededPerTeam;

