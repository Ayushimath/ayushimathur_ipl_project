
function bowlerBestEconomyInSuperOver(deliveries) {
    let bowlerData = {}
    for (let eachdelivery of deliveries) {
        let bowler = eachdelivery.bowler;
       

        if (eachdelivery["is_super_over"] == 1) {
            let totalRun = parseInt(eachdelivery.total_runs);
            let byeRun = parseInt(eachdelivery.bye_runs);
            let legbyeRun = parseInt(eachdelivery.legbye_runs);
            let penaltyRun = parseInt(eachdelivery.penalty_runs);

            let wide = parseInt(eachdelivery.wide_runs);
            let noball = parseInt(eachdelivery.noball_runs);

            if (!(bowler in bowlerData)) {
                bowlerData[bowler] = { runs: 0, balls: 0 };

                bowlerData[bowler]["runs"] += totalRun - byeRun - legbyeRun - penaltyRun;
                if (wide == 0 && noball == 0) {
                    bowlerData[bowler]["balls"] += 1

                }
            }
            else if (bowler in bowlerData) {
                bowlerData[bowler]["runs"] += totalRun - byeRun - legbyeRun - penaltyRun;
                if (wide == 0 && noball == 0) {
                    bowlerData[bowler]["balls"] += 1

                }
            }
        }
    }
    let bowlerEconomy = {}
    for (let eachplayer in bowlerData) {

        bowlerEconomy[eachplayer] =
            parseFloat(((bowlerData[eachplayer]["runs"] /
                bowlerData[eachplayer]["balls"]) * 6).toFixed(3))


    }
    const bestBowlerEconomy=Object.entries(bowlerEconomy).sort(function(a,b){
        return a[1]-b[1];
    })
    return bestBowlerEconomy[0];
}

module.exports = bowlerBestEconomyInSuperOver;