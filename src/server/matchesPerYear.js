//const lib = require('./index.js');

//console.log(matches)

function matchesPerYear(matches) {
    result = []
    object = {}
    object["season"] = 0;
    object["matches"] = 0;

    //iterating through object in array
    for (let i = 0; i < matches.length; i++) {

        //if our object is empty,simply add season and matches
        if (!object["season"] && !object["matches"]) {
            object["season"] = matches[i]["season"]
            object["matches"] = 1;
        }//if season matched the season in object
        else if (matches[i]["season"] === object["season"]) {
            object["matches"] = object["matches"] + 1;

        }//if season didnt match then push previous in result and take a new object
        else {
            result.push(object);
            object = {};
            object["season"] = matches[i]["season"];
            object["matches"] = 1;
        }
        //for including the last object
        if(i+1===matches.length){
            result.push(object);
        }
   }
   function makeobj(fresult,eachdata){
    fresult[eachdata.season]=eachdata["matches"];
    return fresult;
   }
   const fresult=result.reduce(makeobj,{});
    return fresult;

}
module.exports=matchesPerYear;

//