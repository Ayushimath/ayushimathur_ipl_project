

function strikeRateBatsmanEachSeason(matches, deliveries) {
    //obj one for runs
    //obj 2 for balls
    //obj 3 for economy
    let seasons = [];
    for (let data of matches) {
        if (!(seasons.includes(data["season"]))) {
            seasons.push(data["season"]);
        }
    }

    //iterating through  in array
    let matchids = [];
    for (let year of seasons) {
        for (let data of matches) {

            if (data["season"] == year) {
                matchids.push(data["id"]);
                //console.log(matchid);
            }
            object1 = {};
            object2 = {};
            object3 = {};
            object3["season"] = data["season"];

            for (let info in deliveries) {
                if (matchids.includes(info["match_id"])) {
                    //console.log(matchid)
                    if (`${info["batsman"]}` in object1) {
                        object1[`${info["batsman"]}`] += parseInt(deliveries[i]["batsman_runs"]);

                    } else {
                        object1[`${info["batsman"]}`] = parseInt(deliveries[i]["batsman_runs"]);
                    }

                    if (`${info["batsman"]}` in object2) {
                        //console.log("helo");
                        if (info["wide_runs"] == 0 && info["noball_runs"] == 0) {
                            object2[`${info["batsman"]}`] += 1;
                        }
                    } else {
                        //console.log('hello');
                        if (info["wide_runs"] == 0 && info["noball_runs"] == 0) {
                            object2[`${info["batsman"]}`] = 1;
                        }
                    }
                }
            }
        }

        for (let property in object1) {
            //console.log(property, object1[property], object2[property]);
            object3[property] = ((object1[property] / object2[property]) * 100).toFixed(3);
        }

    }

    return object3;
}
module.exports = strikeRateBatsmanEachSeason;



function strikeRateBatsmanEachSeason(matches, deliveries) {
    let record = {};
    for (let eachdelivery of deliveries) {
        let batsman = eachdelivery.batsman;
        let deliveryid = eachdelivery.match_id;
        let batsmanruns = parseInt(eachdelivery.batsman_runs);
        let wide = parseInt(eachdelivery.wide_runs);
        let noball = parseInt(eachdelivery.noball_runs);

        for (let eachmatch of matches) {
            let year = eachmatch.season;
            let matchid = eachmatch.id;

            if (deliveryid == matchid) {
                if (!(record.hasOwnProperty(batsman))) {
                    record[`${batsman}`] = {}
                    record[`${batsman}`][`${year}`] = {runs: 0, balls: 0 };


                    record[batsman][year]["runs"] += batsmanruns;
                    if (wide == 0 && noball == 0) {
                        record[batsman][year]["balls"] += 1;
                    }
                }
                else if (record.hasOwnProperty(batsman)) {
                    if (record[batsman].hasOwnProperty(year)) {
                        record[batsman][year]["runs"] += batsmanruns;
                        if (wide == 0 && noball == 0) {
                            record[batsman][year]["balls"] += 1;

                        }

                    }
                    else if (!(record[batsman].hasOwnProperty(year))) {
                        record[batsman][`${year}`] = { runs: 0, balls: 0 };
                        record[batsman][year]["runs"] += batsmanruns;
                        if (wide == 0 && noball == 0) {
                            record[batsman][year]["balls"] += 1;

                        }
                    }

                }
            }
        }
    }
    let eachPlayerStrikeRate={};
    for(let eachPlayer in record){
        let eachPlayerYear={};
       // console.log(eachPlayer);
        
        for(let eachYear in record[eachPlayer]){

            eachPlayerYear[`${eachYear}`]=
            parseFloat((((record[eachPlayer][eachYear]["runs"])/(record[eachPlayer][eachYear]["balls"]))*100).toFixed(3));

        }
        eachPlayerStrikeRate[`${eachPlayer}`]=eachPlayerYear;

    }
    
    return eachPlayerStrikeRate;
}
module.exports = strikeRateBatsmanEachSeason;



