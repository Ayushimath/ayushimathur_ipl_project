
function playerOfTheMatchForEachSeason(matches) {

    let result = {};
    for (let data of matches) {
        let year = data.season;
        let playerofmatch = data.player_of_match;
        if (!(year in result)) {
            result[year] = {}
            result[year][playerofmatch] = 1

        } else if (year in result) {
            if (playerofmatch in result[year]) {
                result[year][playerofmatch] += 1
            }
            else if (!(playerofmatch in result[year]))
                result[year][playerofmatch] = 1
        }
    }
    let final = {};
    for (let year in result) {
        yeararray = Object.entries(result[year]).sort(function (a, b) {
            return b[1] - a[1];
        });
        final[year] = yeararray[0][0];

    }
return final;
}


module.exports = playerOfTheMatchForEachSeason;