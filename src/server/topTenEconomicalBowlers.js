
function topTenEconomicalBowlers(matches, deliveries, year) {
    //obj one for runs
    //obj 2 for balls
    //obj 3 for economy
    let object1 = {};
    let object2 = {};
    let object3 = {};

    //iterating through  in array
    let matchid;
    for (let j = 0; j < matches.length; j++) {
        if (matches[j]["season"] == year) {
            matchid = matches[j]["id"]
            //console.log(matchid);

            for (let i = 0; i < deliveries.length; i++) {
                if (deliveries[i]["match_id"] === matchid) {
                    //console.log(matchid)
                    if (`${deliveries[i]["bowler"]}` in object1) {
                        object1[`${deliveries[i]["bowler"]}`] += (parseInt(deliveries[i]["total_runs"]) - parseInt(deliveries[i]["bye_runs"]) - (parseInt(deliveries[i]["legbye_runs"]) - parseInt(deliveries[i]["penalty_runs"])));

                    } else {
                        object1[`${deliveries[i]["bowler"]}`] = (parseInt(deliveries[i]["total_runs"]) - parseInt(deliveries[i]["bye_runs"]) - parseInt(deliveries[i]["legbye_runs"] - parseInt(deliveries[i]["penalty_runs"])));
                    }

                    if (`${deliveries[i]["bowler"]}` in object2) {
                        //console.log("helo");
                        if (deliveries[i]["wide_runs"] == 0 && deliveries[i]["noball_runs"] == 0) {
                            object2[`${deliveries[i]["bowler"]}`] += 1;
                        }
                    } else {
                        //console.log('hello');
                        if (deliveries[i]["wide_runs"] == 0 && deliveries[i]["noball_runs"] == 0) {
                            object2[`${deliveries[i]["bowler"]}`] = 1;
                        }
                    }
                }
            }
        }
    }

    for (let property in object1) {
        //console.log(property, object1[property], object2[property]);
        object3[property] = (object1[property] / (object2[property] / 6)).toFixed(2);
    }
    //console.log(object3)
    const result = Object.entries(object3).sort(function (a, b) {
        return a[1] - b[1];
    });


    //printin top fresult
    //console.log(result);
    let fresult = {};
    for (let j = 0; j < 10; j++) {
        fresult[result[j][0]]=parseFloat(result[j][1]);
    }
    return (fresult);
}
module.exports = topTenEconomicalBowlers;









