
function teamWonTossWonMatch(matches) {

    let result={}
    for (let data of matches) {
        let tosswinner=data.toss_winner;
        let winner=data.winner
        if(tosswinner===winner){
            if(winner in result){
                result[winner] +=1
            } else{
                result[winner] =1
            }

        }

    }
    return result;
}
module.exports=teamWonTossWonMatch;